var express = require('express');
var router = express.Router();
const monguse = require("mongoose");
var keygen = require("keygenerator");
const config = require("config");
const Keys = require("../models/key");
const File = require("../models/file");
const formidable = require('formidable');
const fs = require('fs');
var logger = require("../logger/logger");
var Zip = require('node-zip');

var zip = new Zip;


/* Connecting to mongoDB Atlas */
monguse.connect(
  config.get("Paths.dbConfig").dbAddress,
  { useNewUrlParser: true },
  err => {
    if (err) {
      console.log("Error connect to database " + err);
      res.status(404).send('Something went wrong!');
    } else {
      console.log("Connected to mongodb");
      logger.info('Successfuly connected to MongoDB Atlas');
    }
  }
);

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(401).send("Unauthorized access! You need api key for accessing this route, key can be generated on route '<api_root_url>/getapikey'");
  logger.info('Unauthorized access on root "/"');
});

/* GET api key to upload and download files */
router.get('/getapikey', function (req, res, next) {

  var Apikey = keygen.session_id();
  var item = {
    Key: Apikey
  };

  let key = new Keys(item);
  logger.info('New object Key created and ready to be saved in database');

  key.save((err, key) => {
    if (err) {
      console.log(err);
      res.status(408).send('Request timeout! Please try again');
      logger.error('Api key is not created');
    } else {
      res.status(200).send("Your api key for uploading and downloading is:    " + Apikey + "     Make sure that you store this key somewhere so you can use it anytime.");
      logger.info('Api key created :' + Apikey);
    }
  });

});

/* Route for uploading files in ../uploads/ and adding data in MongoDB Atlas  */
router.post('/uploadfile/:apiKey', function (req, res, next) {
  logger.info('Route started /updatefile/:apiKey');
  var apikey = req.params.apiKey;
  console.log(apikey);
  if (apikey.length === 0) {
    res.status(402).send("Unauthorized request!");
    logger.error('Api key was not passed in path');
  }
  Keys.find({ Key: apikey }).exec(function (err, key) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    }
    if (key.length === 0) {
      res.status(404).send("Bad api key!");
    }
    else {
      logger.info('Api key exist in database!');
      const form = formidable({ multiples: true, uploadDir: config.get("Paths.pathForUpload").fileAddress });
      form.parse(req, (err, fields, files) => {
        console.log(files);
        var obj = {
          name: files.file1.name,
          size: files.file1.size,
          extension: files.file1.type,
          time: files.file1.lastModifiedDate,
          Key: apikey
        }
        logger.info('File uploaded!');
        fs.rename(files.file1.path, config.get("Paths.pathForUpload").fileAddress + files.file1.name, function (err) {
          if (err) {
            res.status(500).send(err);
            logger.error('File not renamed!');
          }
          logger.info('File renamed to original name!');
        });
        let file = new File(obj);
        file.save((err, file) => {
          if (err) {
            console.log(err);
          } else {
            console.log(file);
            res.status(200).send("File successfuly uploaded with ID: " + file._id);
            logger.info('Data about file uploaded in database!');
          }
        });
      });
    }
  });
});

/* Route for downloading file/files */
router.post('/downloadfile/:apiKey', function (req, res, next) {
  var apikey = req.params.apiKey;
  var files = req.body;

  if (apikey.length === 0 || files.length === 0) {
    res.status(404).send("Bad api key or ID of file!");
    logger.error('Bad api key or ID of file!');
  }

  console.log(files.length);

  if (files.length > 1) {
    logger.info('Preparing to download .zip file!');

    File.find({ Key: apikey, _id: files }).exec(function (err, data) {
      logger.info('All files for download are found !');
      data.forEach(d => {
        zip.file(d.name, fs.readFileSync(config.get("Paths.pathForDownload").fileAddress + d.name));
        logger.info('Adding file with name:' + d.name + ' into Zip');
      });
      var options = { base64: false, compression: 'DEFLATE' };
      var data = zip.generate(options);
      fs.writeFile('./tmpStorage/tmpData_' + apikey + '.zip', zip.generate(options), 'binary', function (error) {
      });
      logger.info('Zip folder created and waiting for download');
    });

    res.download(config.get("Paths.tmpStoragePath").fileAddress + 'tmpData_' + apikey + '.zip');
    logger.info('File successfuly downloaded');
  } else {
    File.find({ Key: apikey, _id: files }).exec(function (err, data) {
      res.download(config.get("Paths.pathForDownload").fileAddress + data[0].name);
    });
    logger.info('File with name: ' + data[0].name + ' is successfuly downloaded!');
  }
});

module.exports = router;
