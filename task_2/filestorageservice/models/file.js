const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const fileShema = new Schema({
  name: String,
  size: String,
  extension:String,
  time: Date,
  Key: String
},{
  collection:'Files'
});

module.exports = mongoose.model("Files",fileShema,"Files");
