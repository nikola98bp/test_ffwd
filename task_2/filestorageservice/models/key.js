const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const keyShema = new Schema({
  Key: String
},{
  collection:'Keys'
});

module.exports = mongoose.model("Keys",keyShema,"Keys");
