var express = require('express'); 
var router = express.Router();
var request = require('request');
var logger = require("../logger/logger");
const config = require("config");

const API_KEY = process.env.API_KEY;

/* GET home page. */
router.get('/', function (req, res, next) {
  var weather;
  res.render('index', { weather: weather });
  logger.info('Render "/" page');
});


/* POST home page. */
router.post('/city', function (req, res) {
  var array = [];
  var city = req.body.city;  
  logger.info('City selected:'+city);
  var url = config.get("Paths.apiConfig").apiAddress + `/2.5/forecast?q=${city}&units=metric&appid=${API_KEY}`; //url for 5days/3hours api

  request(url, function (error, response, body) { 
    logger.info('Retrieving api data from openweathermap.org');
    if (error) {
      console.log(error);
      logger.error('Could not retrieve api data from openweathermap.org');
    }

    weather = JSON.parse(body); 
    logger.info('Retrieved data parsed to JSON format')

    for (var i = 0; i < 5; i++) {
      var minTemp = +Infinity;
      var maxTemp = -Infinity;
      var tmpObj = {
        date: weather.list[i * 8].dt_txt, 
        icon: weather.list[i * 4].weather[0].icon 
      }
      for (var j = 0; j < 8; j++) {
        if (minTemp > weather.list[i * 8 + j].main.temp_min) { minTemp = weather.list[i * 8 + j].main.temp_min; }
        if (maxTemp < weather.list[i * 8 + j].main.temp_max) { maxTemp = weather.list[i * 8 + j].main.temp_max; }
      }

      tmpObj.temp_min = minTemp;
      tmpObj.temp_max = maxTemp;

      array.push(tmpObj); // pushing objects in array 
    }
    logger.info('Filtering data from api into array of objects: Successful ->'+JSON.stringify(array));
    logger.info('Rendering index page with weather property'); 
    res.render('index', { weather: array }); 
  });

});

module.exports = router;
